import React  from 'react';
import { useNavigation } from '@react-navigation/native';
import {
    View,
    Text,
    Button
} from 'react-native';

export default function SampleHome(props) {
    const navigation = useNavigation();
    return (
        <View>
            <Text>This is is a sample home view</Text>
            <Button title="Go to Device Information" onPress={() => navigation.navigate('DeviceInformation')}></Button>
            <Button title="Go to Location information" onPress={() => navigation.navigate('LocationInformation')}></Button>
        </View>
    )
}