import React, { useState, useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import Geolocation from '@react-native-community/geolocation';
import {
    View,
    Text,
    Button,
    PermissionsAndroid,
    Platform
} from 'react-native';

export default function LocationData(props) {
    const [permission, setPermission] = useState(undefined);
    const [coordinates, setCoords] = useState({ latitude: undefined, longitude: undefined })
    const navigation = useNavigation();
    console.log('testlog', Platform.OS, permission);
    const requestLocationPermission = async () => {
        try {
            console.log("try request")
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    'title': 'SampleApp',
                    'message': 'Sample App wants access to your location',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can use the location");
                setPermission(true);
            } else {
                console.log("location permission denied");
            }
        } catch (err) {
            console.warn(err)
        }
    }
    React.useEffect(() => {

        //iOS and Android require different methods to obtain authorization
        if (!permission && (Platform.OS == 'ios')) {
            Geolocation.requestAuthorization();
            setPermission(true);
            return;
        }
        if (!permission && (Platform.OS == 'android')) {
            requestLocationPermission();
            return;
        }

        if ((coordinates.longitude == undefined) && (coordinates.longitude == undefined)) {
            Geolocation.getCurrentPosition(info => {
                console.log(info);
                setCoords({ latitude: info.coords.latitude, longitude: info.coords.longitude })
            });
        }
    });

    return (
        <View>
            <Text>Geolocation data:</Text>
            {((coordinates.latitude) && coordinates.longitude) &&
                <View>
                    <Text>Your coordinates are:</Text>
                    <Text> Latitude: {coordinates.latitude} </Text>
                    <Text>Longitude: {coordinates.longitude}</Text>
                </View>}
            {(!coordinates.latitude && coordinates.longitude) &&
                <Text>Sorry, could not get your coordinates.</Text>
            }
            <Button title="Go back" onPress={() => navigation.goBack()}></Button>
        </View>
    );
}