import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import DeviceInformation from './DeviceData';
import LocationInformation from './LocationData';
import SampleHome from './SampleHome';

const Stack = createStackNavigator();

export default function MainStackNavigator() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    name="Home"
                    options={{ title: 'Welcome' }}
                    component={SampleHome}
                />
                <Stack.Screen
                    name="DeviceInformation"
                    options={{ title: 'Device Information' }}
                    component={DeviceInformation}
                />
                <Stack.Screen
                    name="LocationInformation"
                    options={{ title: 'Location Information' }}
                    component={LocationInformation}
                />
            </Stack.Navigator>
        </NavigationContainer >
    )
}