import React from 'react';
import { useNavigation} from '@react-navigation/native';
import DeviceInfo from 'react-native-device-info';
import {
    View,
    Text,
    Button
} from 'react-native';

ShowOSVersion = () => {
    let OSVersion = DeviceInfo.getSystemVersion();

    return (
        <Text>Operating System Version: {OSVersion}</Text>
    )
}

ShowOS = () => {
    let OS = DeviceInfo.getSystemName();
    return (
        <Text>Operating System: {OS}</Text>
    )
}

const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
function niceBytes(x) {

    let l = 0, n = parseInt(x, 10) || 0;

    while (n >= 1024 && ++l) {
        n = n / 1024;
    }
    //include a decimal point and a tenths-place digit if presenting 
    //less than ten of KB or greater units
    return (n.toFixed(n < 10 && l > 0 ? 1 : 0) + ' ' + units[l]);
}

export default DeviceInformation = (props) => {
    const [usedMemory, setUsedMemory] = React.useState("Calculating...");
    const [totalMemory, setTotalMemory] = React.useState("Retrieving...");
    const [deviceId, setDeviceId] = React.useState("Retrieving...");
    const navigation = useNavigation();

    const getUsedMemory = async () => {
        try {
            const response = await DeviceInfo.getUsedMemory();
            // console.log(response);
            convertedRes = niceBytes(response);
            setUsedMemory(convertedRes);
            return convertedRes;
        } catch (e) {
            alert(e);
        }
    }

    const getTotalMemory = async () => {
        try {
            const response = await DeviceInfo.getTotalMemory();
            convertedRes = niceBytes(response);
            setTotalMemory(convertedRes);
            return convertedRes;
        } catch (e) {
            alert(e);
        }
    }

    const getDeviceId = async () => {
        try {
            const response = await DeviceInfo.getDeviceId();
            setDeviceId(response);
            return response;
        } catch (e) {
            alert(e);
        }
    }

    React.useEffect(() => {
        if (usedMemory == "Calculating..." || !usedMemory) {
            getUsedMemory();
        }
        if (totalMemory == "Retrieving..." || !totalMemory) {
            getTotalMemory();
        }
        if (deviceId == "Retrieving..." || !deviceId) {
            getDeviceId();
        }
    });

    // setUsedMemory(getUsedMemory());
    //usedMemory == 'Calculating...' ? setUsedMemory(getUsedMemory()) : 'Error while retrieving';
    return (
        <View>
            <Text>DeviceInfo: </Text>
            <ShowOS></ShowOS>
            <ShowOSVersion></ShowOSVersion>
            <Text>Total available memory: {totalMemory}</Text>
            <Text>Amount of memory used: {usedMemory}</Text>
            <Text>Device ID: {deviceId}</Text>
            <Button title="Go back" onPress={() => navigation.goBack()}></Button>
        </View>
    );
}
