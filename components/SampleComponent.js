import React, { Component } from 'react';
import { useNavigation, NavigationContainer } from '@react-navigation/native';
import DeviceInfo from 'react-native-device-info';
import {
    View,
    Text,
    Button
} from 'react-native';

export default function SampleComponent(props) {
    const navigation = useNavigation();
    return (
        <View>
            <Text>This is text from my own component. With edit or two.</Text>
            <Button title="Go back" onPress={() => navigation.goBack()}></Button>
        </View>
    );
}